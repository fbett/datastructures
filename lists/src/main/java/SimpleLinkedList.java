import java.util.Iterator;

public class SimpleLinkedList<E> implements Iterable <E>, MyList<E> {

    private int count;
    private Node root;


    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public void add(E i) {
        Node<E> current = root;
        Node<E> prev = null;
        while(current != null) {
            prev = current;
            current = current.next;
        }
        Node<E> newNode = new Node<>(i);
        if (prev != null) {
            prev.next = newNode;
        } else {
            root = newNode;
        }
        count ++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public E get(int ix) {
        checkRange(ix);
        Node<E> p = root;
        for (int i = 0; i < ix; i++) {
            p = p.next;
        }
        return p.value;
    }

    private void checkRange(int ix) {
        if (ix < 0 || ix >= count) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");

        Node p = root;
        while (p != null) {
            sb.append(p.value).append(",");
            p = p.next;
        }

        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean contains(E i) {
        for( Node<E> p = root; p != null; p = p.next) {
            if (p.value == i) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new SimpleListIterator(root);
    }

    @Override
    public void remove(int ix) {
        checkRange(ix);
        Node<E> current = root;
        Node<E> prev = null;
        for (int i = 0; i < ix; i++) {
            prev = current;
            current = current.next;
        }
        if (prev != null) {
            prev.next = current.next;
        } else {
            root = current.next;
        }

        count--;
    }

    @Override
    public void add(int ix, E x) {
        if (ix < 0 || ix > count) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> current = root;
        Node<E> prev = null;
        for (int i = 0; i < ix; i++) {
            prev = current;
            current = current.next;
        }
        Node<E> newNode = new Node<>(x);
        newNode.next = current;
        if (prev != null) {
            prev.next = newNode;
        } else {
            root = newNode;
        }

        count++;
    }

    private static class SimpleListIterator<E> implements Iterator<E> {

        private Node<E> current;

        public SimpleListIterator(Node<E> root) {
            current = root;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            E value = current.value;
            current = current.next;
            return value;
        }
    }

    private static class Node <E> {
        E value;
        Node next;

        public Node(E value) {
            this.value = value;
        }
    }
}
