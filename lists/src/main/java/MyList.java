import java.util.Iterator;

public interface MyList<E> extends Iterable<E> {

    boolean isEmpty();

    void add(E i);

    int size();

    E get(int ix);

    @Override
    String toString();

    boolean contains(E i);

    Iterator iterator();

    void remove(int ix);

    void add(int ix, E x);
}
