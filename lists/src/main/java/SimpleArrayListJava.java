import java.util.AbstractList;
import java.util.Iterator;

public class SimpleArrayListJava<E> extends AbstractList<E>  {

    private int count;
    private E[] items;

    public SimpleArrayListJava() {
        items = (E[]) new Object[10];
    }

    private void ensureCapacity() {
        if (count == items.length) {
            E[] aux = (E[]) new Object[items.length * 2];
            System.arraycopy(items, 0, aux, 0, count);
            items = aux;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public E get(int ix) {
        checkRange(ix);
        return items[ix];
    }

    @Override
    public E set(int index, E element) {
        checkRange(index);
        E old = items[index];
        items[index] = element;
        return old;
    }


    private void checkRange(int ix) {
        if (ix < 0 || ix >= count) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public E remove(int ix) {
        checkRange(ix);
        E old = items[ix];
        System.arraycopy(items, ix + 1, items, ix, count - ix - 1);
        count--;
        items[count] = null;
        return old;
    }

    @Override
    public void add(int ix, E x) {
        if (ix < 0 || ix > count) {
            throw new IndexOutOfBoundsException();
        }
        ensureCapacity();
        System.arraycopy(items, ix, items, ix + 1, count - ix);
        items[ix] = x;
        count++;
    }

}
