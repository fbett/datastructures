import java.util.Iterator;

public class SimpleArrayList<E> implements Iterable <E>, MyList<E> {

    private int count;
    private E[] items;

    public SimpleArrayList() {
        items = (E[]) new Object[10];
    }


    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public void add(E x) {
        ensureCapacity();
        items[count] = x;
        count ++;
    }

    private void ensureCapacity() {
        if (count == items.length) {
            E[] aux = (E[]) new Object[items.length * 2];
            System.arraycopy(items, 0, aux, 0, count);
            items = aux;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public E get(int ix) {
        checkRange(ix);
        return items[ix];
    }

    private void checkRange(int ix) {
        if (ix < 0 || ix >= count) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");

        for (E x: this) {
            sb.append(x).append(",");
        }

        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean contains(E x) {
        for (int i = 0; i < count; i++) {
            if (x.equals(items[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new SimpleListIterator();
    }

    @Override
    public void remove(int ix) {
        checkRange(ix);
        System.arraycopy(items, ix + 1, items, ix, count - ix - 1);
        count--;
        items[count] = null;
    }

    @Override
    public void add(int ix, E x) {
        if (ix < 0 || ix > count) {
            throw new IndexOutOfBoundsException();
        }
        ensureCapacity();
        System.arraycopy(items, ix, items, ix + 1, count - ix);
        items[ix] = x;
        count++;
    }

    private class SimpleListIterator<E> implements Iterator<E> {

        private int current;

        public SimpleListIterator() {

        }

        @Override
        public boolean hasNext() {
            return current < count;
        }

        @Override
        public E next() {
            E value = (E) items[current];
            current++;
            return value;
        }
    }
}
