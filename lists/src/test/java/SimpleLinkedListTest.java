import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static java.lang.Integer.valueOf;
import static org.junit.Assert.*;

public class SimpleLinkedListTest {

    private MyList<Integer> simpleList;

    @Before
    public void setup() {
        simpleList = new SimpleArrayList<>();
    }

    @Test
    public void whenEmpty_thenIsEmptyIsTrue() {
        assertTrue(simpleList.isEmpty());
    }

    @Test
    public void whenEmpty_thenSizeIsZero() {
        assertEquals(0, simpleList.size());
    }

    @Test
    public void whenAddSeveral_thenSizeChanges() {
        simpleList.add(1);
        assertEquals(1, simpleList.size());
        simpleList.add(2);
        assertEquals(2, simpleList.size());
    }

    @Test
    public void whenNotEmpty_thenIsEmptyIsFalse() {
        simpleList.add(1);
        assertFalse(simpleList.isEmpty());
    }

    @Test
    public void whenGetIsCalled_thenElementInPositionReturned() {
        simpleList.add(1);
        assertEquals(valueOf(1), simpleList.get(0));
        simpleList.add(42);
        assertEquals(valueOf(42), simpleList.get(1));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void whenGetIsCalledWithNegative_thenThrow() {
        simpleList.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void whenGetIsCalledWithBiggerThanCount_thenThrow() {
        simpleList.get(1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void whenRemoveIsCalledWithNegative_thenThrow() {
        simpleList.remove(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void whenRemoveIsCalledWithBiggerThanCount_thenThrow() {
        simpleList.remove(1);
    }

    @Test
    public void whenToStringIsCalled_thenStringIsReturned() {
        simpleList.add(1);
        simpleList.add(2);
        simpleList.add(3);
        assertEquals("[1,2,3,]", simpleList.toString());
        assertEquals("[1,2,3,]", simpleList.toString());
    }

    @Test
    public void testGet() {
        int count = 10;
        for (int i = 0; i < count; i++) {
            simpleList.add(i);
        }

        for (int i = 0; i < simpleList.size(); i++) {
            assertEquals(valueOf(i), simpleList.get(i));
        }
    }

    @Test
    public void testContains() {
        assertFalse(simpleList.contains(42));
        simpleList.add(3);
        simpleList.add(42);
        simpleList.add(5);
        assertTrue(simpleList.contains(42));
    }


    @Test
    public void testAddIndex() {
        for (int i = 0; i < 10; i++) {
            simpleList.add(i + 1);
        }
        int actual = simpleList.size();
        simpleList.add(4, 42);
        assertEquals("[1,2,3,4,42,5,6,7,8,9,10,]", simpleList.toString());
        assertEquals(actual + 1, simpleList.size());
        simpleList.add(0, 41);
        assertEquals("[41,1,2,3,4,42,5,6,7,8,9,10,]", simpleList.toString());
        assertEquals(actual + 2, simpleList.size());
        simpleList.add(simpleList.size(), 43);
        assertEquals("[41,1,2,3,4,42,5,6,7,8,9,10,43,]", simpleList.toString());
        assertEquals(actual + 3, simpleList.size());

    }


    @Test
    public void testRemoveIndex() {
        for (int i = 0; i < 10; i++) {
            simpleList.add(i);
        }
        int actual = simpleList.size();
        simpleList.remove(4);
        assertEquals("[0,1,2,3,5,6,7,8,9,]", simpleList.toString());
        assertEquals(actual - 1, simpleList.size());
    }

    @Test
    public void testRemoveIndexLast() {
        for (int i = 0; i < 10; i++) {
            simpleList.add(i);
        }
        int actual = simpleList.size();
        simpleList.remove(9);
        assertEquals("[0,1,2,3,4,5,6,7,8,]", simpleList.toString());
        assertEquals(actual - 1, simpleList.size());
    }

    @Test
    public void testRemoveFirst() {
        simpleList.add(42);
        assertFalse(simpleList.isEmpty());
        simpleList.remove(0);
        assertTrue(simpleList.isEmpty());
        assertEquals("[]", simpleList.toString());
    }


    @Test
    public void testIterator() {
        int count = 100;
        for (int i = 0; i < count; i++) {
            simpleList.add(i);
        }

        Iterator it = simpleList.iterator();
        int i = 0;
        while (it.hasNext()) {
            int next = (int) it.next();
            assertEquals(i, next);
            i++;
        }
        assertEquals(count, i);

        i = 0;
        for(Integer n:simpleList) {
            assertEquals(i, (int)n);
            i++;
        }
        assertEquals(count, i);

    }

    @Test
    public void insertFirst() {
        int count = 100000;
        for (int i = 0; i < count; i++) {
            simpleList.add(0, i);
        }
        assertEquals(count, simpleList.size());
    }


}
